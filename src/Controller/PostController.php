<?php

namespace App\Controller;

use App\Entity\Post;
use App\Formatter\PostFormatter;
use App\Repository\PostRepository;
use App\Validation\PostValidation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/post')]
class PostController extends AbstractController
{
    #[Route('/', methods: ['GET'])]
    public function index(PostRepository $postRepository, PostFormatter $formatter): JsonResponse
    {
        $posts = $postRepository->findAll();

        $formattedPosts = [];

        foreach ($posts as $post) {
            $formattedPosts[] = $formatter->format($post);
        }

        return $this->json(['posts' => $formattedPosts]);
    }

    #[Route('/new', methods: ['POST'])]
    public function new(
        Request $request,
        ValidatorInterface $validator,
        PostRepository $postRepository,
        PostFormatter $formatter
    ): Response {
        $requestData = json_decode($request->getContent(), true);
        $validation = new PostValidation($requestData['title'] ?? null, $requestData['content'] ?? null);

        $errors = $validator->validate($validation);

        if (count($errors) > 0) {
            return $this->json(['errors' => (string) $errors], Response::HTTP_BAD_REQUEST);
        }

        $post = new Post($validation->getTitle(), $validation->getContent());

        $postRepository->save($post, true);

        return $this->json($formatter->format($post));
    }

    #[Route('/{id}', methods: ['GET'])]
    public function show(Post $post, PostFormatter $formatter): Response
    {
        return $this->json($formatter->format($post));
    }

    #[Route('/{id}/edit', methods: ['PUT', 'POST'])]
    public function edit(
        Request $request,
        Post $post,
        PostRepository $postRepository,
        PostFormatter $formatter
    ): Response {
        if ($request->get('title')) {
            $post->setTitle($request->get('title'));
        }

        if ($request->get('content')) {
            $post->setContent($request->get('content'));
        }

        $postRepository->save($post, true);

        return $this->json($formatter->format($post));
    }

    #[Route('/{id}', methods: ['DELETE'])]
    public function delete(Post $post, PostRepository $postRepository, PostFormatter $formatter): Response
    {
        $postRepository->remove($post, true);

        return $this->json($formatter->format($post));
    }
}
