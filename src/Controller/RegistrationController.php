<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Validation\UserValidation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationController extends AbstractController
{
    #[Route('/api/register', methods: ['POST'])]
    public function register(
        Request $request,
        ValidatorInterface $validator,
        UserPasswordHasherInterface $passwordHasher,
        UserRepository $userRepository
    ): JsonResponse {
        $requestData = json_decode($request->getContent(), true);
        $validation = new UserValidation($requestData['email'] ?? null, $requestData['password'] ?? null);

        $errors = $validator->validate($validation);
        if (count($errors) > 0) {
            return $this->json(['errors' => (string) $errors], Response::HTTP_BAD_REQUEST);
        }

        $user = new User();

        $hashedPassword = $passwordHasher->hashPassword(
            $user,
            $validation->getPassword()
        );

        $user->setPassword($hashedPassword);
        $user->setEmail($validation->getEmail());

        $userRepository->save($user, true);

        return $this->json(['success' => true]);
    }
}
