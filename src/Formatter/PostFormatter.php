<?php

declare(strict_types=1);

namespace App\Formatter;

use App\Entity\Post;

class PostFormatter
{
    public function format(Post $post): array
    {
        return [
            'id' => $post->getId(),
            'title' => $post->getTitle(),
            'content' => $post->getContent(),
            'created_at' => $post->getCreatedAt()?->format(DATE_ATOM),
            'updated_at' => $post->getUpdatedAt()?->format(DATE_ATOM),
        ];
    }
}
