<?php

declare(strict_types=1);

namespace App\Validation;

use Symfony\Component\Validator\Constraints as Assert;

class PostValidation
{
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private ?string $title;

    #[Assert\NotBlank]
    private ?string $content;

    public function __construct(?string $title, ?string $content)
    {
        $this->title = $title;
        $this->content = $content;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
